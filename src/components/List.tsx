import React from "react";
import "./List.css"
import '../isVisible'
import isVisible from "../isVisible";

export default function List(props) {
function checkElement(){
  const elem = document.querySelectorAll('.list-elem')[props.data.length-1];
  if(isVisible(elem)) props.getNextRows();
}


    return (
  
      <div className="list" onScroll={checkElement}>
  
        {props.data.map((pokemon) => (
  
          <div
  
            className="list-elem"
  
            key={pokemon.name}
  
            onClick={() => props.getPokemonData(pokemon.name.toString())}
  
          >
  
            {pokemon.name}
  
          </div>
  
        ))}
  
      </div>
  
    );
  
  }
