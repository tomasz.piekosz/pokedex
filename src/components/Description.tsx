import React, {useState} from "react";
import "./Description.css"

export default function Description(props) {
  let [position, changePosition] = useState('front_default');

  const interval_id = window.setInterval(function(){}, Number.MAX_SAFE_INTEGER);

  // Clear any timeout/interval up to that id
  for (let i = 1; i < interval_id; i++) {
    window.clearInterval(i);
  }
  
  const interval = setInterval(function () {
      if(position === 'front_default') changePosition('back_default');
      else changePosition('front_default')
    }, 800);



    if(props.selectedPokemonData) return (
      
      <div className="description">
  
        <img src={props.selectedPokemonData.sprites[position]}></img>
        <div className="title">{props.selectedPokemonData.name}</div>
        <div className="info">Type: {props.selectedPokemonData.types[0].type.name}</div>
        <div className="info">Height: {props.selectedPokemonData.height}</div>
        <div className="info">Weight: {props.selectedPokemonData.weight}</div>


      </div>
  
    );

  }