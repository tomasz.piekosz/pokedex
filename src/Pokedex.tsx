import React, { Component } from "react";

import List from "./components/List"
import Description from "./components/Description";

import "./Pokedex.css";
import fetchData from "./fetchData";

export default class Pokedex extends Component<{}, { pokemonList: any, loading: boolean, selectedPokemonData: any, offset: number }> {

  constructor(props) {

    super(props);

    this.state = {

      pokemonList: [],

      loading: true,

      selectedPokemonData: null,
      offset: 0,

    };

  }

  fetchList = async () =>{
    this.setState({ loading: true });
    const data:any = await fetchData('?limit=20');
    this.setState({
      pokemonList: data.results,
      loading: false,
    })
  }


  componentDidMount() {
    this.fetchList();
  }


  getPokemonData(pokemon: string) {

    fetchData('/'+pokemon).then((data) => {
        this.setState({

          selectedPokemonData: data,

        }); 
      });

  }


    getNextRows(){
      if(this.state.loading) return;
  
      this.setState({ loading: true });
      fetchData(`?limit=20&offset=${this.state.offset+20}`).then((data) => {
        this.setState({
  
          pokemonList: this.state.pokemonList.concat(...data.results),
  
          loading: false,
  
          offset: this.state.offset+20,
        });
      })
    console.log(this.state.pokemonList);
    }
 

  render() {



    let content: React.ReactNode = null;

    if (this.state.loading) content = <div className="loading">Loading...</div>;
  
    return (


      <div className="container">
        {content}

        <div>

          <div className="list-title">Choose your Pokemon!</div>

          <List

            data={this.state.pokemonList}

            getPokemonData={this.getPokemonData.bind(this)}

            getNextRows={this.getNextRows.bind(this)}

          ></List>

        </div>

        <Description

          selectedPokemonData={this.state.selectedPokemonData}

        ></Description>
        
      </div>

    );

  }
}


 