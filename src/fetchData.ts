export default async function fetchData(endPoint: string){
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon${endPoint}`);
    return await response.json();
  }
  
